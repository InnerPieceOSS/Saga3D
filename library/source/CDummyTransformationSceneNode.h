// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_DUMMY_TRANSFORMATION_SCENE_NODE_H_INCLUDED__
#define __C_DUMMY_TRANSFORMATION_SCENE_NODE_H_INCLUDED__

#include "IDummyTransformationSceneNode.h"

namespace saga
{
namespace scene
{

  class CDummyTransformationSceneNode : public IDummyTransformationSceneNode
  {
  public:

    //! constructor
    CDummyTransformationSceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id);

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! Returns a reference to the current relative transformation matrix.
    //! This is the matrix, this scene node uses instead of scale, translation
    //! and rotation.
    virtual glm::mat4& getRelativeTransformationMatrix() override;

    //! Returns the relative transformation of the scene node.
    virtual glm::mat4 getRelativeTransformation() const override;

    //! does nothing.
    virtual void render() override {}

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::DUMMY_TRANSFORMATION; }

    //! Creates a clone of this scene node and its children.
    virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;


  private:

    // TODO: We can add least add some warnings to find troubles faster until we have
    // fixed bug id 2318691.
    virtual const glm::vec3& getScale() const override;
    virtual void setScale(const glm::vec3& scale) override;
    virtual const glm::vec3& getRotation() const override;
    virtual void setRotation(const glm::vec3& rotation) override;
    virtual const glm::vec3& getPosition() const override;
    virtual void setPosition(const glm::vec3& newpos) override;

    glm::mat4 RelativeTransformationMatrix;
    core::aabbox3d<float> Box;
  };

} // namespace scene
} // namespace saga

#endif

