// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BILLBOARD_SCENE_NODE_H_INCLUDED__
#define __C_BILLBOARD_SCENE_NODE_H_INCLUDED__

#include "IBillboardSceneNode.h"
#include "SMeshBuffer.h"

namespace saga
{
namespace scene
{

//! Scene node which is a billboard. A billboard is like a 3d sprite: A 2d element,
//! which always looks to the camera.
class CBillboardSceneNode : virtual public IBillboardSceneNode
{
public:

  //! constructor
  CBillboardSceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
    const glm::vec3& position, const glm::vec2& size,
    video::SColor colorTop=video::SColor(0xFFFFFFFF),
    video::SColor colorBottom=video::SColor(0xFFFFFFFF));

  virtual ~CBillboardSceneNode();

  //! pre render event
  virtual void onRegisterSceneNode(video::RenderPassHandle pass) override;

  //! render
  virtual void render() override;

  //! returns the axis aligned bounding box of this node
  virtual const core::aabbox3d<float>& getBoundingBox() const override;

  //! sets the size of the billboard
  virtual void setSize(const glm::vec2& size) override;

  //! Sets the widths of the top and bottom edges of the billboard independently.
  virtual void setSize(float height, float bottomEdgeWidth, float topEdgeWidth) override;

  //! gets the size of the billboard
  virtual const glm::vec2& getSize() const override;

  //! Gets the widths of the top and bottom edges of the billboard.
  virtual void getSize(float& height, float& bottomEdgeWidth, float& topEdgeWidth) const override;

  // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

  //! returns amount of materials used by this scene node.
  virtual std::uint32_t getMaterialCount() const override;

  //! Set the color of all vertices of the billboard
  //! \param overallColor: the color to set
  virtual void setColor(const video::SColor& overallColor) override;

  //! Set the color of the top and bottom vertices of the billboard
  //! \param topColor: the color to set the top vertices
  //! \param bottomColor: the color to set the bottom vertices
  virtual void setColor(const video::SColor& topColor,
      const video::SColor& bottomColor) override;

  //! Gets the color of the top and bottom vertices of the billboard
  //! \param[out] topColor: stores the color of the top vertices
  //! \param[out] bottomColor: stores the color of the bottom vertices
  virtual void getColor(video::SColor& topColor,
      video::SColor& bottomColor) const override;

  //! Get the real boundingbox used by the billboard (which depends on the active camera)
  virtual const core::aabbox3d<float>& getTransformedBillboardBoundingBox(const saga::scene::ICameraSceneNode* camera) override;

  //! Writes attributes of the scene node.
  virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

  //! Reads attributes of the scene node.
  virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

  //! Returns type of the scene node
  virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::BILLBOARD; }

  //! Creates a clone of this scene node and its children.
  virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

protected:
  void updateMesh(const saga::scene::ICameraSceneNode* camera);

private:

  //! Size.Width is the bottom edge width
  glm::vec2 Size;
  float TopEdgeWidth;

  //! BoundingBox which is large enough to contain the billboard independent of the camera
  // TODO: BUG - still can be wrong with scaling < 1. Billboards should calculate relative coordinates for their mesh
  // and then use the node-scaling. But needs some work...
  /** Note that we can't use the real boundingbox for culling because at that point
      the camera which is used to calculate the billboard is not yet updated. So we only
      know the real boundingbox after rendering - which is too late for culling. */
  core::aabbox3d<float> BBoxSafe;

  scene::SMeshBuffer* Buffer;
};


} // namespace scene
} // namespace saga

#endif

