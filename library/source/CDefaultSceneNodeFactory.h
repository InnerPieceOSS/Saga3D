// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_DEFAULT_SCENE_NODE_FACTORY_H_INCLUDED__
#define __C_DEFAULT_SCENE_NODE_FACTORY_H_INCLUDED__

#include "ISceneNodeFactory.h"



namespace saga
{
namespace scene
{
  class ISceneNode;
  class ISceneManager;

  //! Interface making it possible to dynamicly create scene nodes and animators
  class CDefaultSceneNodeFactory : public ISceneNodeFactory
  {
  public:

    CDefaultSceneNodeFactory(ISceneManager* mgr);

    //! adds a scene node to the scene graph based on its type id
    /** \param type: Type of the scene node to add.
    \param parent: Parent scene node of the new node, can be null to add the scene node to the root.
    \return Returns pointer to the new scene node or null if not successful. */
    virtual ISceneNode* addSceneNode(E_SCENE_NODE_TYPE type, ISceneNode* parent= 0) override;

    //! adds a scene node to the scene graph based on its type name
    /** \param typeName: Type name of the scene node to add.
    \param parent: Parent scene node of the new node, can be null to add the scene node to the root.
    \return Returns pointer to the new scene node or null if not successful. */
    virtual ISceneNode* addSceneNode(const char* typeName, ISceneNode* parent= 0) override;

    //! returns amount of scene node types this factory is able to create
    virtual std::uint32_t getCreatableSceneNodeTypeCount() const override;

    //! returns type name of a creatable scene node type by index
    /** \param idx: Index of scene node type in this factory. Must be a value between 0 and
    uetCreatableSceneNodeTypeCount() */
    virtual const char* getCreateableSceneNodeTypeName(std::uint32_t idx) const override;

    //! returns type of a creatable scene node type
    /** \param idx: Index of scene node type in this factory. Must be a value between 0 and
    getCreatableSceneNodeTypeCount() */
    virtual E_SCENE_NODE_TYPE getCreateableSceneNodeType(std::uint32_t idx) const override;

    //! returns type name of a creatable scene node type
    /** \param idx: Type of scene node.
    \return: Returns name of scene node type if this factory can create the type, otherwise 0. */
    virtual const char* getCreateableSceneNodeTypeName(E_SCENE_NODE_TYPE type) const override;

  private:

    E_SCENE_NODE_TYPE getTypeFromName(const char* name) const;

    struct SSceneNodeTypePair
    {
      SSceneNodeTypePair(E_SCENE_NODE_TYPE type, const char* name)
        : Type(type), TypeName(name)
      {}

      E_SCENE_NODE_TYPE Type;
      std::string TypeName;
    };

    std::vector<SSceneNodeTypePair> SupportedSceneNodeTypes;

    ISceneManager* Manager;
  };


} // namespace scene
} // namespace saga

#endif

