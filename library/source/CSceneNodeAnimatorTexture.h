// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_NODE_ANIMATOR_TEXTURE_H_INCLUDED__
#define __C_SCENE_NODE_ANIMATOR_TEXTURE_H_INCLUDED__


#include "ISceneNodeAnimatorFinishing.h"

namespace saga
{
namespace scene
{
  class CSceneNodeAnimatorTexture : public ISceneNodeAnimatorFinishing
  {
  public:

    //! constructor
    CSceneNodeAnimatorTexture(const std::vector<video::ITexture*>& textures,
      std::int32_t timePerFrame, bool loop, std::uint32_t now);

    //! destructor
    virtual ~CSceneNodeAnimatorTexture();

    //! animates a scene node
    virtual void animateNode(ISceneNode* node, std::uint32_t timeMs) override;

    //! Writes attributes of the scene node animator.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node animator.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Returns type of the scene node animator
    virtual ESCENE_NODE_ANIMATOR_TYPE getType() const override { return ESNAT_TEXTURE; }

    //! Creates a clone of this animator.
    /** Please note that you will have to drop
    (IReferenceCounted::drop()) the returned pointer after calling
    this. */
    virtual ISceneNodeAnimator* createClone(ISceneNode* node, ISceneManager* newManager= 0) override;


  private:

    void clearTextures();

    std::vector<video::ITexture*> Textures;
    std::uint32_t TimePerFrame;
    bool Loop;
  };


} // namespace scene
} // namespace saga

#endif

