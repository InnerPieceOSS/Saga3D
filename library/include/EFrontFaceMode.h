#ifndef __E_FRONT_FACE_MODE_H_INCLUDED__
#define __E_FRONT_FACE_MODE_H_INCLUDED__

namespace saga
{
namespace video
{

enum E_FRONT_FACE_MODE
{
  CLOCKWISE,
  COUNTER_CLOCKWISE
};

} // namespace scene
} // namespace saga

#endif

