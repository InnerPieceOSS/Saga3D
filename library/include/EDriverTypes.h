#ifndef __E_DRIVER_TYPES_H_INCLUDED__
#define __E_DRIVER_TYPES_H_INCLUDED__

namespace saga
{
namespace video
{
  //! An enum class for all types of drivers Saga3D supports.
  enum class E_DRIVER_TYPE
  {
    //! Vulkan driver, available on most desktop platforms.
    /** Performs hardware accelerated rendering of 3D and 2D. */
    VULKAN,

    //! Vulkan headless driver, useful for applications to run graphics tasks without creating application window.
    /** This driver is able to render to texture or compute, but will not presenting anything, **/
    /** can be used in an environment where windowing system is not available, i.e.: cloud server. **/
    VULKAN_HEADLESS,

    //! Vulkan driver, similar to E_DRIVER_TYPE::VULKAN but render and present to a window.
    /** Use this when you want to overlay graphics on other application. */
    /** Notes for iOS (and other platforms that can display Vulkan texture directly): use VULKAN_HEADLESS and send your texture handle instead. */
    VULKAN_OVERLAY,

    //! WebGPU driver for rendering on Website.
    WEB_GPU,
  };

  const char* const DRIVER_TYPE_NAMES[] =
  {
    "VulkanDriver",
    "VulkanHeadlessDriver",
    "VulkanOverlayDriver",
    "WebGPUDriver"
  };

  const char* const DRIVER_TYPE_NAMES_SHORT[] =
  {
    "vk",
    "vkhl",
    "vko",
    "wgpu"
  };

} // namespace video
} // namespace saga

#endif // __E_DRIVER_TYPES_H_INCLUDED__
