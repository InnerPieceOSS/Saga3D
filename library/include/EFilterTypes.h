#ifndef __E_FILTER_TYPES_H_INCLUDED__
#define __E_FILTER_TYPES_H_INCLUDED__

namespace saga
{
namespace video
{

enum class E_FILTER_TYPE {
  NEAREST,
  LINEAR,
  CUBIC
};

} // namespace video
} // namespace saga

#endif // __E_FILTER_TYPES_H_INCLUDED__

