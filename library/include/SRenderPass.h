#ifndef __SRENDER_PASS_H_INCLUDED__
#define __SRENDER_PASS_H_INCLUDED__

#include "SGPUResource.h"
#include "STexture.h"
#include "SRenderPassState.h"

namespace saga
{
namespace video
{

  struct SRenderPass : public SGPUResource
  {
    std::array<STexture::HandleType, MAX_COLOR_ATTACHMENTS> ColorAttachments = { NULL_GPU_RESOURCE_HANDLE };
    STexture::HandleType DepthStencilAttachment;
    SRenderPassState State;
    bool UseDefaultAttachments = true;
    bool UpdateAttachments = false;
    bool DrawGeometry = true;
    int AttachmentCount = 0;
    int Width = 0;
    int Height = 0;
  };

  using RenderPassHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SRENDER_PASS_H_INCLUDED__

