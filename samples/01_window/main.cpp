#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);

  auto& driver = device->getVideoDriver();
  auto passInfo = driver->createRenderPass();

  passInfo.State.Colors[0] = {
    E_ATTACHMENT_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  passInfo.DrawGeometry = false;
  
  auto pass = driver->createResource(std::move(passInfo));

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    // save performance by not running always
    // only renders if window is active on screen
    if (device->isWindowActive()) {
      driver->begin();
      driver->beginPass(pass);
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else // else do nothing (pause for 500ms)
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Window - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

