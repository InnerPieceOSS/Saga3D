include_guard()

function(create_sample name sources)
  add_executable(${name})
  target_sources(${name} PRIVATE ${sources})
  target_link_libraries(${name}
    PRIVATE Saga3D::Library
  )
  install(TARGETS ${name}
    DESTINATION bin
  )
  if (MSVC)
    find_package(SDL2 REQUIRED)
    target_link_libraries(${name}
      PRIVATE Saga3D::Library SDL2::SDL2main
    )
  endif()
endfunction()
